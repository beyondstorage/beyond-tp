module github.com/beyondstorage/beyond-tp

go 1.16

require (
	github.com/99designs/gqlgen v0.13.0
	github.com/Xuanwo/go-bufferpool v0.1.0
	github.com/beyondstorage/go-service-azblob/v2 v2.1.0
	github.com/beyondstorage/go-service-cos/v2 v2.1.0
	github.com/beyondstorage/go-service-dropbox/v2 v2.1.0
	github.com/beyondstorage/go-service-fs/v3 v3.2.0
	github.com/beyondstorage/go-service-gcs/v2 v2.1.0
	github.com/beyondstorage/go-service-kodo/v2 v2.1.0
	github.com/beyondstorage/go-service-oss/v2 v2.1.0
	github.com/beyondstorage/go-service-qingstor/v3 v3.1.0
	github.com/beyondstorage/go-service-s3/v2 v2.2.0
	github.com/beyondstorage/go-storage/v4 v4.2.0
	github.com/beyondstorage/go-toolbox v0.0.0-20210525101004-db84362b9d67
	github.com/dgraph-io/badger/v3 v3.2011.1
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.7.2
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/golang/protobuf v1.5.2
	github.com/google/uuid v1.2.0
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.2
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/spf13/cobra v1.1.3
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.7.0
	github.com/stretchr/testify v1.7.0
	github.com/vektah/gqlparser/v2 v2.1.0
	go.uber.org/zap v1.16.0
	google.golang.org/grpc v1.38.0
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.1.0
	google.golang.org/protobuf v1.26.0
)
